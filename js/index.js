var departmentValues = [];

function validation(event) {
    event.preventDefault();
    var firstName = document.forms['registration-form']['first-name'];
    var lastName = document.forms['registration-form']['last-name'];
    var email = document.forms['registration-form']['email'];
    var contact = document.forms['registration-form']['contact'];
    var address = document.forms['registration-form']['address'];
    var checkboxes = document.getElementById('department-select');
    var registraionDate = document.forms['registration-form']['date-of-registration'];
    var comment = document.forms['registration-form']['comment'];
    var thankScreen = document.querySelector('.thanks-screen');
    var formAll = document.forms['registration-form']

    //firstname validation
    if (firstName.value == '') {
        firstName.classList.add('alert-danger');
        firstName.focus();
        document.getElementById('first-error').innerHTML = 'Please Enter First Name';
        return false;
    }
    else {
        document.getElementById('first-error').innerHTML = '';
        firstName.classList.remove('alert-danger');
    }

    //contact validation
    if (contact.value == '' || contact.value.length < 9 || isNaN(contact.value) == true) {
        contact.classList.add('alert-danger');
        contact.focus();
        document.getElementById('contact-error').innerHTML = 'Please Enter Phone Number';
        return false;
    }
    else {
        contact.classList.remove('alert-danger');
        document.getElementById('contact-error').innerHTML = '';
    }

    //email validation
    if (email.value == '' || email.value.indexOf('@', 0) < 0 || email.value.indexOf('.', 0) < 2 || email.value.length < 4) {
        email.classList.add('alert-danger')
        email.focus();
        document.getElementById('email-error').innerHTML = 'Please Enter valid Email';
        return false;
    }
    else {
        email.classList.remove('alert-danger')
        document.getElementById('email-error').innerHTML = '';
    }

    //date validation
    if (registraionDate.value == '' || registraionDate.value.length < 0) {
        registraionDate.classList.add('alert-danger')
        registraionDate.focus();
        document.getElementById('date-error').innerHTML = 'Please select valid Date';
        return false
    }
    else {
        document.getElementById('date-error').innerHTML = '';
    }

    //department validation
    if (departmentValues.length < 1) {
        checkboxes.classList.add('alert-danger')
        checkboxes.focus();
        document.getElementById('department-error').innerHTML = 'Please Select Department Name';
        return false
    }
    else {
        checkboxes.classList.remove('alert-danger')
        document.getElementById('department-error').innerHTML = '';
    }

    console.log('successfully filled', firstName.value, lastName.value, departmentValues, email.value, contact.value, address.value, registraionDate.value)

    thankScreen.style.display = 'block';
    formAll.style.display = 'none';
    return true

}

//for checkboxes dropdown

function selectCheckBox(e) {
    if (e.target.checked) {
        departmentValues.push(e.target.value);
    }
    else {
   
        if (departmentValues.indexOf(e.target.value) > -1) {
            var c = e.target.value;
            var position = departmentValues.indexOf(c);
            departmentValues.splice(position, 1)
            console.log('delete', departmentValues)
        }
    }
    displaySelected();
}

function displaySelected() {
	debugger
    let a = '';
   
    departmentValues.map(val => {
        a += ` <span> ${val}</span>`
    })
    document.getElementById('depart-Name').innerHTML = a;

}


// welcome screen back button
function showform() { 
    var thankScreen = document.querySelector('.thanks-screen');
    var checkboxes = document.getElementById('depart-Name');
    var formAll = document.forms['registration-form'];
    thankScreen.style.display = 'none';
    formAll.style.display = 'block';
    checkboxes.textContent ='None Selected'
    resetAll();
}

//reset
function resetAll() {  
    document.forms['registration-form'].reset();
    departmentValues.length = 0;
    var removeAllerror = document.querySelectorAll(".alert-danger");
    for(var i =0;i<removeAllerror.length;i++){
    removeAllerror[i].classList.remove('alert-danger');
    }
  
    var removeNote = document.querySelectorAll('.error')
    for(var i = 0;i<removeNote.length;i++){
        removeNote[i].innerHTML = '';
        }
        var checkboxes = document.getElementById('depart-Name');
        checkboxes.textContent ='None Selected';
    displaySelected()
}

//checkbox multi-select and values
window.addEventListener('DOMContentLoaded', (event) => {
    var dropbox = document.querySelector('.depart-con');
    dropbox.addEventListener('click', function (e) {
        if (e.currentTarget.classList.contains('openselect')) {
            e.stopPropagation();
        }
    });

    var checkboxes = document.forms['registration-form'].elements['department[]'];
    for (var i = 0, len = checkboxes.length; i < len; i++) {
        checkboxes[i].addEventListener('change', (e) => {
            selectCheckBox(e)
        });
    }
});